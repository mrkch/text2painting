#!/bin/bash

cd img2txt

python3 run_inference_batch.py \
--checkpoint_path="checkpoints/model.ckpt-2000000" \
--vocab_file="checkpoints/word_counts.txt" \
--input_dir="wikiart/Baroque"  \
--output_file="out_baroque.txt"