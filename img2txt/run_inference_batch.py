# Copyright 2016 The TensorFlow Authors. All Rights Reserved.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
# ==============================================================================
r"""Generate captions for images using default beam search parameters."""

from __future__ import absolute_import
from __future__ import division
from __future__ import print_function

import math
import os


import tensorflow as tf

import configuration
import inference_wrapper
from inference_utils import caption_generator
from inference_utils import vocabulary

FLAGS = tf.flags.FLAGS

tf.flags.DEFINE_string("checkpoint_path", "",
                       "Model checkpoint file or directory containing a "
                       "model checkpoint file.")
tf.flags.DEFINE_string("vocab_file", "", "Text file containing the vocabulary.")
tf.flags.DEFINE_string("input_dir", "",
                       "Directory with the image files to caption.")
tf.flags.DEFINE_string("output_file", "",
                       "File to write the captions into")
tf.logging.set_verbosity(tf.logging.INFO)


def main(_):
  # Build the inference graph.
  g = tf.Graph()
  with g.as_default():
    model = inference_wrapper.InferenceWrapper()
    restore_fn = model.build_graph_from_config(configuration.ModelConfig(),
                                               FLAGS.checkpoint_path)
  g.finalize()

  # Create the vocabulary.
  vocab = vocabulary.Vocabulary(FLAGS.vocab_file)

  filenames = list( os.listdir(FLAGS.input_dir))

  tf.logging.info("Running caption generation on %d files matching %s",
                  len(filenames), FLAGS.input_dir)
  
  img_captions = {}


  with tf.Session(graph=g) as sess:    
    # Load the model from checkpoint.
    restore_fn(sess)

    # Prepare the caption generator. Here we are implicitly using the default
    # beam search parameters. See caption_generator.py for a description of the
    # available beam search parameters.
    generator = caption_generator.CaptionGenerator(model, vocab)
    with open(FLAGS.output_file, "a") as o:
      for filename in filenames:      
        img_captions[filename] = []
        print(f"Captioning image '{filename}'.")
        # try:
        with tf.gfile.GFile(os.path.join(FLAGS.input_dir, filename), "rb") as f:
          image = f.read()
        captions = generator.beam_search(sess, image)
        for i, caption in enumerate(captions):
          # Ignore begin and end words.
          sentence = [vocab.id_to_word(w) for w in caption.sentence[1:-1]]
          sentence = " ".join(sentence)
          #print("  %d) %s (p=%f)" % (i, sentence, math.exp(caption.logprob)))
          img_captions[filename].append((sentence, math.exp(caption.logprob)))

        line = filename
        l = img_captions[filename]

        for s, p in l:
          line += f"|{s}|{str(p)}"

        o.write(line + "\n")
        # except:
        #   print("Error.")       

if __name__ == "__main__":
  tf.app.run()
