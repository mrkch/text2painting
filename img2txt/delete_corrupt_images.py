import os
from PIL import Image

corrupt = []
path = "wikiart/Baroque"

for filename in os.listdir(path):
    try:
        Image.open(os.path.join(path,filename))
    except:
        corrupt.append(filename)
        print(filename)