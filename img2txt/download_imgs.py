import requests
import json
import shutil
from requests.sessions import session 
from tqdm import tqdm
import os
import argparse

SESSION_FILE = "session_token"

ACCESS = "002646cbc8314853"
SECRET = "b25cf7f10b711e1f"

artists = []
session = ""

# SESSION
def create_session():
    '''
    Initialize session. T
    his function can only be run 10 times per hour (check MaxRequestsPerHour).
    It generates a session token if there is none and returns it.
    One does not seem to need the token in the requests, however, still do it according to
    https://docs.google.com/document/d/1T926unU7mx9Blmx3c8UE0UQTnO3MrDbXTGYVerVQFDU (old)
    https://docs.google.com/document/d/1Vxi5lQnMCA21dvNm_7JVd6nQkDS3whV3YjRjbwWPfQU (recent)
    '''
    print("Create session.")
    if os.path.exists(SESSION_FILE):
        session = ""
        with open(SESSION_FILE, "r") as f:
            return f.read()
    else:
        data = requests.get(f'https://www.wikiart.org/en/Api/2/login?accessCode={ACCESS}&secretCode={SECRET}')

        session = data.json()["SessionKey"]


        max_requests_per_hour = data.json()["MaxRequestsPerHour"]
        max_sessions_per_hour = data.json()["MaxSessionsPerHour"]

        print(f"Saved new session token to '{SESSION_FILE}'")
        print(f"Session token:          {session}")
        print(f"Max requests per hour:  {max_requests_per_hour}")
        print(f"Max sessions per hour:  {max_sessions_per_hour}")

        with open(SESSION_FILE, "w") as f:
            f.write(session)

    print(f"Established session with token '{session}'")
    return session

# ARTISTS
def get_artists(only_public_domain = True):
    artists_data = requests.get(f"http://www.wikiart.org/en/App/Artist/AlphabetJson?v=new&inPublicDomain={only_public_domain}")
    artists = artists_data.json()
    print(f"Found {len(artists)} artists.")
    return artists

def find_artist_by_name(name):
    global artists
    l = []
    for a in artists:
        if name in a["artistName"]:
            l.append(a)

    if len(l) > 1:
        print("Found more than one artist with that name. Possible matches:")
        print(list(map(lambda x : x["artistName"], l)))
        return None
    else:
        return l[0]

def get_artist_url(artist):
    return artist["url"]
  
def get_artist_name(artist):
    return artist["artistName"]

# IMAGES
def get_images_by_artist(name):
    artist = find_artist_by_name(name)
    
    if not artist:
        print("No artist selected.")
        return None

    url = get_artist_url(artist)
    
    img_data = requests.get(f"http://www.wikiart.org/en/App/Painting/PaintingsByArtist?artistUrl={url}&json=2")
    
    imgs = img_data.json()
    print(f"Found {len(imgs)} images by {get_artist_name(artist)}.")
    return imgs

def get_image_url(image):
    return image["image"]

# DOWNLOAD
def download(url, filename):
    # Open the url image, set stream to True, this will return the stream content.
    r = requests.get(url, stream = True)

    # Check if the image was retrieved successfully
    if r.status_code == 200:
        # Set decode_content value to True, otherwise the downloaded image file's size will be zero.
        r.raw.decode_content = True
        
        # Open a local file with wb ( write binary ) permission.        
        with open(filename,'wb') as f:
            shutil.copyfileobj(r.raw, f)        
    else:
        print(f'Image Could not be retreived: {url}')

def download_imgs_by_artist(name, output_folder, max = -1):
    imgs = get_images_by_artist(name)

    if max != -1 and 0 < max < len(imgs):
        imgs = imgs[0:max]

    print(f"Will download {len(imgs)} image(s).")

    if not imgs:
        return None

    print("Getting urls.")

    urls = list(map(lambda i : get_image_url(i), imgs))
    
    print("Starting download.")

    for url in tqdm(urls):
        filename = url.split("/")[-1].split("!")[0]
        download(url, os.path.join(output_folder, filename))

def main():
    global session, artists
    
    parser = argparse.ArgumentParser()
    parser.add_argument("output", help="The (relative) path to the folder where to download the images to.")
    parser.add_argument("artist", help="The name (sur- or prename) of the artist by whom you want to download images.")
    parser.add_argument("-max", help="The maximal number of images to be downloaded. If not set, download all images by the given artist.", default=-1, type=int)

    args = parser.parse_args()

    session = create_session()
    artists = get_artists()

    download_imgs_by_artist(args.artist, args.output, args.max)

if __name__ == "__main__":
    main()