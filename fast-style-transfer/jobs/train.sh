#!/bin/bash

cd fast-style-transfer

python -m venv venv --system-site-packages
. venv/bin/activate
pip install -r requirements.txt


# Set pytorch model zoo download path to something with write access
export TORCH_HOME=home

filename="jobs/train_styles.txt"
while read -r line; do
    style_path="styles/$line"
    python neural_style.py train --dataset data --style-image $style_path --save-model-dir models --cuda 1
done < "$filename"
