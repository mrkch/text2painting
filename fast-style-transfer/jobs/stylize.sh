#!/bin/bash

cd fast-style-transfer

python -m venv venv --system-site-packages
. venv/bin/activate
pip install -r requirements.txt

python neural_style.py eval-batch --content-dir data/train2014 --max-num 100 --output-dir stylized --model models/monet_00.model --cuda 1