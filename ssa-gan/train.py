from __future__ import print_function
import multiprocessing

import os
import io
import sys
import time
import errno
import random
import pprint
import datetime
import dateutil.tz
import argparse

import torch
import torchvision
import torch.nn as nn
import torch.optim as optim
from torch.autograd import Variable
import torch.backends.cudnn as cudnn
import torchvision.transforms as transforms
import torchvision.utils as vutils
from tensorboardX import SummaryWriter
import numpy as np
from PIL import Image
from tqdm import tqdm
import matplotlib.pyplot as plt

from miscc.utils import mkdir_p
from miscc.utils import imagenet_deprocess_batch
from miscc.config import cfg, cfg_from_file
from miscc.losses import DAMSM_loss
from sync_batchnorm import DataParallelWithCallback
#from datasets_everycap import TextDataset
from datasets import TextDataset
from datasets import prepare_data
from DAMSM import RNN_ENCODER, CNN_ENCODER
from model import NetG, NetD

from argparse import ArgumentParser


if __name__ == "__main__":

    parser = ArgumentParser()

    parser.add_argument('--config', type=str,
                        dest='config',
                        help='Path to config file',
                        default='cfg/wikiart.yml',
                        required=False)

    parser.add_argument('--text_encoder', type=str,
                        dest='text_encoder_path',
                        help='Path of the text encoder weights',
                        default='pretrained/coco/text_encoder_120.pth',
                        required=False)

    parser.add_argument('--image_encoder', type=str,
                        dest='image_encoder_path',
                        help='Path of the image encoder weights',
                        default='pretrained/coco/image_encoder_120.pth',
                        required=False)

    parser.add_argument('--netG', type=str,
                        dest='model_G_path',
                        help='Path of the generator weights',
                        default='pretrained/coco/netG_120.pth',
                        required=False)

    parser.add_argument('--netD', type=str,
                        dest='model_D_path',
                        help='Path of the discriminator weights',
                        default='pretrained/coco/netD_120.pth',
                        required=False)

    parser.add_argument('--checkpoints', type=str,
                        dest='checkpoint_path',
                        help='Path where checkpoints are saved',
                        default='checkpoints',
                        required=False)

    opts = parser.parse_args()


    seed = 100
    num_workers = 3

    cfg_from_file(opts.config)

    random.seed(seed)
    np.random.seed(seed)
    torch.manual_seed(seed)
    torch.cuda.manual_seed_all(seed)

    cudnn.benchmark = True

    # Get data loader
    imsize = cfg.TREE.BASE_SIZE
    batch_size = cfg.TRAIN.BATCH_SIZE
    image_transform = transforms.Compose([
            transforms.Resize(int(imsize * 76 / 64)),
            transforms.RandomCrop(imsize),
            transforms.RandomHorizontalFlip()])

    dataset = TextDataset(cfg.DATA_DIR, 'train',
                        base_size=cfg.TREE.BASE_SIZE,
                        transform=image_transform)
    ixtoword = dataset.ixtoword
    print(dataset.n_words, dataset.embeddings_num)
    assert dataset
    dataloader = torch.utils.data.DataLoader(dataset, 
                                            batch_size=batch_size, 
                                            drop_last=True,
                                            shuffle=True, 
                                            num_workers=num_workers)

    device = torch.device("cuda" if torch.cuda.is_available() else "cpu")

    netG = NetG(cfg.TRAIN.NF, 100).to(device)
    netD = NetD(cfg.TRAIN.NF).to(device)
    netG = DataParallelWithCallback(netG)
    netD = nn.DataParallel(netD)

    text_encoder = RNN_ENCODER(dataset.n_words, nhidden=cfg.TEXT.EMBEDDING_DIM)
    #state_dict = torch.load(text_encoder_path, map_location=lambda storage, loc: storage)
    #text_encoder.load_state_dict(state_dict)
    text_encoder.cuda()

    image_encoder = CNN_ENCODER(cfg.TEXT.EMBEDDING_DIM)
    #state_dict = torch.load(img_encoder_path, map_location=lambda storage, loc: storage)
    #image_encoder.load_state_dict(state_dict)
    image_encoder.cuda()

    # get parameters from text_encoder and image_encoder
    encoder_parameters = list(text_encoder.parameters())
    for v in image_encoder.parameters():
        if v.requires_grad:
            encoder_parameters.append(v)
    optimizerEncoder = torch.optim.Adam(encoder_parameters, lr=0.00004, betas=(0.0, 0.9))

    state_epoch = 0

    optimizerG = torch.optim.Adam(netG.parameters(), lr=0.0001, betas=(0.0, 0.9))
    optimizerD = torch.optim.Adam(netD.parameters(), lr=0.0004, betas=(0.0, 0.9))


    # Restore models
    netG.load_state_dict(torch.load(opts.model_G_path))
    netD.load_state_dict(torch.load(opts.model_D_path))
    text_encoder.load_state_dict(torch.load(opts.text_encoder_path))
    image_encoder.load_state_dict(torch.load(opts.image_encoder_path))
    netG.train()
    netD.train()
    text_encoder.train()
    image_encoder.train()

    istart = cfg.TRAIN.NET_G.rfind('_') + 1
    iend = cfg.TRAIN.NET_G.rfind('.')
    state_epoch = int(cfg.TRAIN.NET_G[istart:iend])


    writer = SummaryWriter('checkpoints/writer')


    def prepare_labels(batch_size):
        # Kai: real_labels and fake_labels have data type: torch.float32
        # match_labels has data type: torch.int64
        real_labels = Variable(torch.FloatTensor(batch_size).fill_(1))
        fake_labels = Variable(torch.FloatTensor(batch_size).fill_(0))
        match_labels = Variable(torch.LongTensor(range(batch_size)))
        if cfg.CUDA:
            real_labels = real_labels.cuda()
            fake_labels = fake_labels.cuda()
            match_labels = match_labels.cuda()
        return real_labels, fake_labels, match_labels

    real_labels, fake_labels, match_labels = prepare_labels(batch_size)

    for epoch in tqdm(range(state_epoch + 1, cfg.TRAIN.MAX_EPOCH + 1)):
        data_iter = iter(dataloader)
        # for step, data in enumerate(dataloader, 0):
        for step in tqdm(range(len(data_iter))):
            data = data_iter.next()

            imags, captions, cap_lens, class_ids, keys = prepare_data(data)
            hidden = text_encoder.init_hidden(batch_size)
            # words_embs: batch_size x nef x seq_len
            # sent_emb: batch_size x nef
            words_embs, sent_emb = text_encoder(captions, cap_lens, hidden)
            words_embs_de, sent_emb_de = words_embs.detach(), sent_emb.detach()

            imgs = imags[0].to(device)
            real_features = netD(imgs)
            output = netD.module.COND_DNET(real_features, sent_emb_de)
            errD_real = torch.nn.ReLU()(1.0 - output).mean()

            output = netD.module.COND_DNET(real_features[:(batch_size - 1)], sent_emb_de[1:batch_size])
            errD_mismatch = torch.nn.ReLU()(1.0 + output).mean()

            # synthesize fake images
            noise = torch.randn(batch_size, 100)
            noise = noise.to(device)
            fake, _ = netG(noise, sent_emb_de)

            # update encoder
            DAMSM_D = DAMSM_loss(image_encoder, imgs, real_labels, words_embs,
                                sent_emb, match_labels, cap_lens, class_ids)
            optimizerEncoder.zero_grad()
            DAMSM_D.backward()
            optimizerEncoder.step()

            # G does not need update with D
            fake_features = netD(fake.detach())

            errD_fake = netD.module.COND_DNET(fake_features, sent_emb_de)
            errD_fake = torch.nn.ReLU()(1.0 + errD_fake).mean()

            errD = errD_real + (errD_fake + errD_mismatch) / 2.0
            optimizerD.zero_grad()
            errD.backward()
            optimizerD.step()

            # MA-GP
            interpolated = (imgs.data).requires_grad_()
            sent_inter = (sent_emb_de.data).requires_grad_()
            features = netD(interpolated)
            out = netD.module.COND_DNET(features, sent_inter)
            grads = torch.autograd.grad(outputs=out,
                                        inputs=(interpolated, sent_inter),
                                        grad_outputs=torch.ones(out.size()).cuda(),
                                        retain_graph=True,
                                        create_graph=True,
                                        only_inputs=True)
            grad0 = grads[0].view(grads[0].size(0), -1)
            grad1 = grads[1].view(grads[1].size(0), -1)
            grad = torch.cat((grad0, grad1), dim=1)
            grad_l2norm = torch.sqrt(torch.sum(grad ** 2, dim=1))
            d_loss_gp = torch.mean((grad_l2norm) ** 6)
            d_loss = 2.0 * d_loss_gp
            optimizerD.zero_grad()
            d_loss.backward()
            optimizerD.step()

            # update G
            features = netD(fake)
            output = netD.module.COND_DNET(features, sent_emb_de)
            errG = - output.mean()
            DAMSM_G = 0.1 * DAMSM_loss(image_encoder, fake, real_labels, words_embs_de,
                                        sent_emb_de, match_labels, cap_lens, class_ids)
            errG_total = errG + DAMSM_G
            optimizerG.zero_grad()
            errG_total.backward()
            optimizerG.step()

        #cap_imgs = cap2img(ixtoword, captions, cap_lens)
        #write_images_losses(writer, cap_imgs, imgs, fake, errD, d_loss, DAMSM_D, errG, DAMSM_G, epoch)
        #write_images_losses(writer, imgs, fake, errD, d_loss, DAMSM_D, errG, DAMSM_G, epoch)

            writer.add_scalar('errD/d_loss', errD, epoch)
            writer.add_scalar('errD/MAGP', d_loss, epoch)
            writer.add_scalar('errD/DAMSM', DAMSM_D, epoch)
            writer.add_scalar('errG/g_loss', errG, epoch)
            writer.add_scalar('errG/DAMSM', DAMSM_G, epoch)

        if (epoch >= cfg.TRAIN.WARMUP_EPOCHS) and (epoch % cfg.TRAIN.GSAVE_INTERVAL == 0):
            torch.save(netG.state_dict(), 'checkpoints/netG_%03d.pth' % (epoch,))
            torch.save(text_encoder.state_dict(), 'checkpoints/text_encoder_%03d.pth' % (epoch,))
        if (epoch >= cfg.TRAIN.WARMUP_EPOCHS) and (epoch % cfg.TRAIN.DSAVE_INTERVAL == 0):
            torch.save(netD.state_dict(), 'checkpoints/netD_%03d.pth' % (epoch,))
            torch.save(image_encoder.state_dict(), 'checkpoints/image_encoder_%03d.pth' % (epoch,))

