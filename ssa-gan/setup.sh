#!/bin/bash

pip3 install gdown --user
pip3 install -r requirements.txt --user

# Download dictionary
gdown --id "1rSnbIGNDGZeHlsUlLdahj0RJ9oo6lgH9" -O coco-captions.zip
unzip coco-captions.zip -d data

# Download pretrained networks
mkdir pretrained
mkdir pretrained/coco
curl -L https://api.onedrive.com/v1.0/shares/u!aHR0cHM6Ly8xZHJ2Lm1zL3UvcyFBc0NxVEFlZ0VER1NkR25jcl8wWi1yclRrMFU/root/content --output pretrained/coco/netG_120.pth
curl -L https://api.onedrive.com/v1.0/shares/u!aHR0cHM6Ly8xZHJ2Lm1zL3UvcyFBc0NxVEFlZ0VER1NmQklBbGVyUFdGN3pkOTQ/root/content --output pretrained/coco/text_encoder_120.pth
curl -L https://api.onedrive.com/v1.0/shares/u!aHR0cHM6Ly8xZHJ2Lm1zL3UvcyFBc0NxVEFlZ0VER1NkbFFHVllTbDNXTWhpSWc_ZT1vYm8yOFk/root/content --output pretrained/coco/netD_120.pth
curl -L https://api.onedrive.com/v1.0/shares/u!aHR0cHM6Ly8xZHJ2Lm1zL3UvcyFBc0NxVEFlZ0VER1NmYVFtS3dHTElLX1lnQXc_ZT1KV3puQW0/root/content --output pretrained/coco/image_encoder_120.pth
