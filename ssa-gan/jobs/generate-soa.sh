#!/bin/bash

cd ssa-gan

python -m venv venv --system-site-packages
. venv/bin/activate
pip install -r requirements.txt

python generate_soa.py --captions soa-captions.txt --model checkpoints/netG_160.pth --text_encoder checkpoints/text_encoder_160.pth
#python generate_soa.py --captions soa-captions.txt --model pretrained/coco/netG_120.pth --text_encoder pretrained/coco/text_encoder_120.pth
