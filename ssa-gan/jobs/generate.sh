#!/bin/bash

cd ssa-gan

python -m venv venv --system-site-packages
. venv/bin/activate
pip install -r requirements.txt

python generate.py "some cows are standing on the field on a sunny day" --model checkpoints/netG_140.pth --text_encoder checkpoints/text_encoder_140.pth