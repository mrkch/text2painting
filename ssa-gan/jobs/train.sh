#!/bin/bash

cd ssa-gan

python -m venv venv --system-site-packages
. venv/bin/activate
pip install -r requirements.txt

# Set pytorch model zoo download path to something with write access
export TORCH_HOME=home

python train.py --text_encoder checkpoints/text_encoder_140.pth --image_encoder checkpoints/image_encoder_140.pth --netG checkpoints/netG_140.pth --netD checkpoints/netD_140.pth
