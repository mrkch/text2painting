from model import NetG, NetD
from DAMSM import RNN_ENCODER
import os
import sys
import time
import random
import pprint
import datetime
import dateutil.tz
import argparse
import io
import errno
from argparse import ArgumentParser

import numpy as np
from PIL import Image
import json
from collections import OrderedDict
import matplotlib.pyplot as plt
import torchvision.transforms as transforms
from tqdm import tqdm

import torch
import torch.nn as nn
import torch.backends.cudnn as cudnn

import pickle

cudnn.benchmark = True


def get_caption_idx(word2idx, max_words_per_cap, caption):
    caption = caption.split(' ')
    caption_idx = []
    for word in caption:
        assert word in word2idx, word + ' is not in word dictionary, please try another word!'
        word_idx = word2idx[word]
        caption_idx.append(word_idx)

    caption = np.asarray(caption_idx).astype('int64')
    if (caption == 0).sum() > 0:
        print('ERROR: do not need END (0) token', caption)
    num_words = len(caption)
    # pad with 0s (i.e., '<end>')
    x = np.zeros((max_words_per_cap, 1), dtype='int64')
    x_len = num_words
    if num_words <= max_words_per_cap:
        x[:num_words, 0] = caption
    else:
        ix = list(np.arange(num_words))  # 1, 2, 3,..., maxNum
        np.random.shuffle(ix)
        ix = ix[:max_words_per_cap]
        ix = np.sort(ix)
        x[:, 0] = caption[ix]
        x_len = max_words_per_cap
    return x, x_len


def mkdir_p(path):
    try:
        os.makedirs(path)
    except:
        pass


def cap2img(ixtoword, caps, cap_lens):
    imgs = []
    for cap, cap_len in zip(caps, cap_lens):
        idx = cap[:cap_len].cpu().numpy()
        caption = []
        for i, index in enumerate(idx, start=1):
            caption.append(ixtoword[index])
            if i % 4 == 0 and i > 0:
                caption.append("\n")
        caption = " ".join(caption)
        fig = plt.figure(figsize=(2.5, 1.5))
        plt.axis("off")
        plt.text(0.5, 0.5, caption)
        plt.xlim(0, 10)
        plt.ylim(0, 10)
        buf = io.BytesIO()
        plt.savefig(buf, format="png")
        plt.close(fig)
        buf.seek(0)
        img = Image.open(buf).convert('RGB')
        img = transforms.ToTensor()(img)
        imgs.append(img)
    imgs = torch.stack(imgs, dim=0)
    assert imgs.dim() == 4, "image dimension must be 4D"
    return imgs



if __name__ == "__main__":

    parser = ArgumentParser()
    parser.add_argument('--captions', type=str,
                        dest='captions',
                        help='Path of captions file',
                        required=True)

    parser.add_argument('--dict', type=str,
                        dest='dict_path',
                        help='.pickle file that contains all captions that were used for training',
                        default='data/coco/captions.pickle',
                        required=False)

    parser.add_argument('--model', type=str,
                        dest='model_path',
                        help='Path of the model weights',
                        default='pretrained/coco/netG_120.pth',
                        required=False)

    parser.add_argument('--text_encoder', type=str,
                        dest='text_encoder_path',
                        help='Path of the text encoder weights',
                        default='pretrained/coco/text_encoder_120.pth',
                        required=False)

    opts = parser.parse_args()

    use_gpu = True

    # manualSeed to control the noise
    manualSeed = 100
    random.seed(manualSeed)
    np.random.seed(manualSeed)
    torch.manual_seed(manualSeed)

    with open(opts.dict_path, 'rb') as f:
        x = pickle.load(f)
        train_captions, test_captions = x[0], x[1]
        ixtoword, wordtoix = x[2], x[3]
        del x
        n_words = len(ixtoword)
        print('Load from: ', opts.dict_path)

    # load rnn encoder
    text_embed_dim = 256
    text_encoder = RNN_ENCODER(n_words, nhidden=text_embed_dim, drop_prob=0)
    state_dict = torch.load(opts.text_encoder_path, map_location=lambda storage, loc: storage)
    text_encoder.load_state_dict(state_dict)

    # load netG
    cond_dim = 100
    state_dict = torch.load(opts.model_path, map_location=torch.device('cpu'))
    netG = NetG(64, cond_dim)
    new_state_dict = OrderedDict()
    for k, v in state_dict.items():
        name = k[7:]  # remove `module.`nvidia
        new_state_dict[name] = v
    model_dict = netG.state_dict()
    pretrained_dict = {k: v for k, v in new_state_dict.items() if k in model_dict}
    model_dict.update(pretrained_dict)
    netG.load_state_dict(model_dict)


    def generate(caption, out_path):
        # generate noise
        num = 1
        noise = torch.FloatTensor(num, 100)

        # convert caption to index
        max_words_per_cap = 100
        caption_idx, caption_len = get_caption_idx(wordtoix, max_words_per_cap, caption.lower())
        caption_idx = torch.LongTensor(caption_idx)
        caption_len = torch.LongTensor([caption_len])
        caption_idx = caption_idx.view(1, -1)
        caption_len = caption_len.view(-1)

        # use gpu or not, change model to evaluation mode
        if use_gpu:
            text_encoder.cuda()
            netG.cuda()
            caption_idx = caption_idx.cuda()
            caption_len = caption_len.cuda()
            noise = noise.cuda()

        text_encoder.eval()
        netG.eval()

        # use rnn encoder to get caption embedding
        hidden = text_encoder.init_hidden(1)
        words_embs, sent_emb = text_encoder(caption_idx, caption_len, hidden)

        mkdir_p(os.path.dirname(out_path))

        # generate fake image
        noise.data.normal_(0, 1)
        sent_emb = sent_emb.repeat(num, 1)
        words_embs = words_embs.repeat(num, 1, 1)
        with torch.no_grad():
            fake_imgs, fusion_mask = netG(noise, sent_emb)

            # save generated images and masks
            im = fake_imgs[0].data.cpu().numpy()
            im = (im + 1.0) * 127.5
            im = im.astype(np.uint8)
            im = np.transpose(im, (1, 2, 0))
            im = Image.fromarray(im)
            im.save(out_path)


    with open(opts.captions, 'r') as f:
        for line in f:
            out_path, caption = line.split('|')
            try:
                generate(caption.lower().rstrip(), out_path)
            except:
                print(out_path, caption)

